package com.example.jeremiah.cf_ramos;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import static com.example.jeremiah.cf_ramos.R.id.celsiusTxt;

public class MainActivity extends AppCompatActivity {

    TextView farenheitTxt;
    EditText celsiusTxt;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        farenheitTxt = (TextView) findViewById(R.id.farenheitTxt);
        celsiusTxt = (EditText) findViewById(R.id.celsiusTxt);

        Button convertBtn = (Button) findViewById(R.id.convertBtn);
        convertBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                float cv = Float.parseFloat(celsiusTxt.getText().toString());
                float fv = ((cv*9)/5) + 32;
                farenheitTxt.setText(Float.toString(fv) + " °F");
            }
        });


    }
}
